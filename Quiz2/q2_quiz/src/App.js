import logo from './logo.svg';
import './App.css';

import Grade from './components/Grade';
import AboutMe from './components/AboutMe';
import Header from './components/Header';
import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={
                  <Grade />
        } />

        <Route path="about" element={
                  <AboutMe />
        } />
      </Routes>
    </div>
  );
}

export default App;
