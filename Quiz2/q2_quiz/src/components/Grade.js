import { useState } from "react";
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function Grade () {

    const [ grades, setGrades ] = useState("");
    const [ cal, setCal ] = useState("");
    const [ hide, setHide ] = useState("");

    function Calculate () {
        let g = parseInt(grades);
        setHide(g);

        if( g < 50 ) {
            setCal("เกรด E");
        }else if ( g < 55 ) {
            setCal("เกรด D");
        }else if ( g < 60 ) {
            setCal("เกรด D+");
        }else if ( g < 70 ) {
            setCal("เกรด C");
        }else if ( g < 75 ) {
            setCal("เกรด C+");
        }else if ( g < 80 ) {
            setCal("เกรด B");
        }else if ( g < 85 ) {
            setCal("เกรด B+");
        }else {
            setCal("เกรด A");
        }
    }

    return (
        <div align="center">
            <Box sx={{ width : "60%" }}>
                <Paper elevation={3}>
                
                    <h1>คำนวณเกรดวิชา 344-243</h1>
                    <h3>กรุณาใส่เกรด ระหว่าง 0-100 : </h3><input type="text" value={grades} onChange={ (e) => { setGrades(e.target.value); }} /> <br></br><br></br>

                    <Button variant="contained" onClick={ () => { Calculate() } }>คำนวณ</Button>
            
                    {   ( hide != 0 ) && 
                            <div>
                                <h2>ผลลัพธ์ : { cal } </h2>
                            </div>
                    }
                </Paper>
            </Box>
        </div>
    );
}

export default Grade;