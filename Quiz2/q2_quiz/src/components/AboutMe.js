import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
function AboutMe () {
    return (
        <div align="center">
            <Box sx={{ width : "60%" }}>
                <Paper elevation={3}>
                    <h2>จัดทำโดย : เศรษฐฉัตร ทุมรัตน์</h2>
                    <h2>รหัสนักศึกษา : 5810210944</h2>
                </Paper>
            </Box>
        </div>
    );
}

export default AboutMe;